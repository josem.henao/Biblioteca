package com.Ventanas;

import java.awt.GridLayout;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.Biblioteca.Biblioteca;
import com.Biblioteca.Escrito;
import com.Biblioteca.Libro;
import com.Principal.Principal;

public class Mostrar extends JOptionPane{
	
	
	public static String pedirTipoEscrito() {
		String opcion = null;
		Object[] opciones = {"Articulo","Ensayo","Libro","Revista"};
		opcion = (String) JOptionPane.showInputDialog(null, "Especifique el tipo de escrito para ingresar", "Tipo de escrito", 3, gaboIcon(), opciones, 0);
//		0 : Articulo
//		1 : Ensayo
//		2 : Libro
//		3 : Revista
		return opcion; 
	}
	
	public static int ventanaPrincipal(Biblioteca biblioteca) {
		int opcion=0;
		String[] opciones = {"Agregar Escrito","Consultar Escrito","Salir"};
		opcion = JOptionPane.showOptionDialog(null, "Que deseas hacer en "+biblioteca.getNombre(), "Ventana Principal",1, 3, gaboIcon(),opciones, 2);
//		0 : Agregar Escrito
//		1 : Consultar Escrito
//		2 : Salir
		return opcion;
	}
	
	public static ImageIcon gaboIcon () {
		return new ImageIcon(Principal.class.getResource("gabo.jpeg"));
	}

	public static Biblioteca pedirDatosLibro(Biblioteca biblioteca) {
		JPanel panel = new JPanel();
		
		String[] autores=listarAutores(biblioteca);
		String[] categorias=listarCategorias(biblioteca);
		String[] editoriales=listarEditoriales(biblioteca);
		
		JComboBox listaAutor = new JComboBox(autores);
		JTextField textfieldISBN = new JTextField();
		JTextField textfieldTitulo= new JTextField();
		JComboBox listaCategoria = new JComboBox(categorias);
		JComboBox listaEditorial = new JComboBox (editoriales);
		JTextField textfieldCantidad= new JTextField();
		
		panel.setLayout(new GridLayout(6, 2, 7, 7));
		
		panel.add(new JLabel("Ingresa el Autor"));
		panel.add(listaAutor);
		panel.add(new JLabel("Ingresa el título"));
		panel.add(textfieldTitulo);
		panel.add(new JLabel("Ingresa el ISBN"));
		panel.add(textfieldISBN);
		panel.add(new JLabel("Ingresa la categoría"));
		panel.add(listaCategoria);
		panel.add(new JLabel("Ingresa la editorial"));
		panel.add(listaEditorial);
		panel.add(new JLabel("Ingresa la cantidad"));
		panel.add(textfieldCantidad);
		
		JOptionPane.showMessageDialog(null, panel, "Datos de un libro", 3, gaboIcon());
		
		String autor= autores[listaAutor.getSelectedIndex()];
		String categoria = categorias[listaCategoria.getSelectedIndex()];
		String editorial= categorias[listaCategoria.getSelectedIndex()];
		
		if (autores[listaAutor.getSelectedIndex()].equals("--Otro--")) {
			autor=JOptionPane.showInputDialog(null, "Ingresa el nombre del Autor", "Nombre del Autor", 1);
		}
		if (editoriales[listaEditorial.getSelectedIndex()].equals("--Otro--")) {
			editorial=JOptionPane.showInputDialog(null, "Ingresa la editorial", "Nombre de la editorial", 1);
		}
		if (categorias[listaCategoria.getSelectedIndex()].equals("--Otro--")) {
			categoria=JOptionPane.showInputDialog(null, "Ingresa la caegoria", "Nombre de la categoría", 1);
		}
		Escrito escrito= new Libro(textfieldTitulo.getText(), textfieldISBN.getText(), autor, categoria,editorial);
		biblioteca.agregarEjemplar(escrito, Integer.parseInt(textfieldCantidad.getText()));
		return biblioteca;
	}
	
	//***************OBTENIENDO LAS LISTAS DE LOS DATOS MASIVOS DE LA BIBLIOTECA***************
	
	private static  String[] listarAutores(Biblioteca biblioteca) {
		String[] escritores = new String[biblioteca.getAutores().size()+1];
		for (int i=0;i<biblioteca.getAutores().size();i++) {
			escritores[i]=biblioteca.getAutores().get(i);
		}
		escritores[biblioteca.getCategorias().size()]="--Otro--";
		return escritores;
	}
	
	private static String[] listarCategorias(Biblioteca biblioteca) {
		String[] categorias = new String[biblioteca.getCategorias().size()+1];
		for (int i=0;i<biblioteca.getCategorias().size();i++) {
			categorias[i]=biblioteca.getCategorias().get(i);
		}
		categorias[biblioteca.getCategorias().size()]="--Otro--";
		return categorias;
	}
	
	private static String[] listarEditoriales(Biblioteca biblioteca) {
		String[] editoriales = new String[biblioteca.getEditoriales().size()+1];
		for (int i=0;i<biblioteca.getEditoriales().size();i++) {
			editoriales[i]=biblioteca.getEditoriales().get(i);
		}
		editoriales[biblioteca.getCategorias().size()]="--Otro--";
		return editoriales;
	}
}
