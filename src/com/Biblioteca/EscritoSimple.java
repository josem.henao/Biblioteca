package com.Biblioteca;

public class EscritoSimple extends Escrito {
	private String version;
	private String autor;
	private String categoria;

	public EscritoSimple(String titulo, String iSBN, String autor, String categoria) {
		super(titulo, iSBN);
		this.autor = autor;
		this.categoria = categoria;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getAutor() {
		return autor;
	}

	public String getCategoria() {
		return categoria;
	}
	
}
