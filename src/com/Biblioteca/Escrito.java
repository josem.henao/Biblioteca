package com.Biblioteca;

public class Escrito {
	private String titulo;
	private String ISBN;
	
	public Escrito(String titulo, String iSBN) {
		super();
		this.titulo = titulo;
		ISBN = iSBN;
	}
	
	public String getTitulo() {
		return titulo;
	}
	public String getISBN() {
		return ISBN;
	}
	
	
}
