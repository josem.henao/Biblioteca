//package com.Biblioteca;
//
//import java.util.ArrayList;
//
//public class Editorial {
//	private String nombre;
//	private String pais;
//	ArrayList<String> categorias = new ArrayList<>();
//	ArrayList<Libro> libros = new ArrayList<>();
//	ArrayList<Revista> revistas = new ArrayList<>();
//	ArrayList<String > autores = new ArrayList<>();
//	
//	public Editorial(String nombre, String pais) {
//		super();
//		this.nombre = nombre;
//		this.pais = pais;
//	}
//	
//	public boolean agregarLibro(Libro libro) {
//		boolean agregado = false;
//		if(!comprobarExistenciaLibro(libro)) {
//			libros.add(libro);
//			agregarCategoria(libro.getCategoria());
//			agregarAutor(libro.getAutor()); 
//			agregado =true;
//		}
//		return agregado;
//	}
//	
//	public boolean agregarRevista(Revista revista) {
//		boolean agregado = false;
//		if(!comprobarExistenciaRevista(revista)) {
//			revistas.add(revista);
//			agregado =true;
//		}
//		return agregado;
//	}
//	
//	private void agregarCategoria(String categoria) {
//		if (!categorias.contains(categoria)) {
//			categorias.add(categoria);
//		}
//	}
//	
//	private void agregarAutor(String autor) {
//		if (!autores.contains(autor)) {
//			autores.add(autor);
//		}
//	}
//	
//	private boolean comprobarExistenciaLibro(Libro libro) {
//		boolean existe=false;
//		for(Libro l: libros) {
//			if(l.getISBN().equals(libro.getISBN())) {
//				existe=true;
//				break;
//			}
//		}
//		return existe;
//	}
//	
//	private boolean comprobarExistenciaRevista(Revista revista) {
//		boolean existe=false;
//		for(Revista r: revistas) {
//			if(r.getISBN().equals(r.getISBN())) {
//				existe=true;
//				break;
//			}
//		}
//		return existe;
//	}
//		
//	public String getNombre() {
//		return nombre;
//	}
//	public void setNombre(String nombre) {
//		this.nombre = nombre;
//	}
//	public String getPais() {
//		return pais;
//	}
//	public void setPais(String pais) {
//		this.pais = pais;
//	}
//	public ArrayList<String> getCategorias() {
//		return categorias;
//	}
//	public void setCategorias(ArrayList<String> categorias) {
//		this.categorias = categorias;
//	}
//
//}