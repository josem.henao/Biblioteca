package com.Biblioteca;

import java.util.ArrayList;
import java.util.HashMap;

import com.Prestado.Prestado;

public class Biblioteca {
	private String nombre;
	private HashMap<String, Integer> ejemplaresISBN = new HashMap<>(); 
	private HashMap<String, Integer> ejemplaresTitulo = new HashMap<>(); 
	private ArrayList<String> autores = new ArrayList<>();
	private ArrayList<Libro> libros = new ArrayList<>();
	private ArrayList<Articulo> articulos = new ArrayList<>();
	private ArrayList<Ensayo> ensayos = new ArrayList<>();
	private ArrayList<Revista> revistas = new ArrayList<>(); 
	private ArrayList<String> editoriales = new ArrayList<>();
	private ArrayList<String> categorias = new ArrayList<>();
	private Prestado prestamos=new Prestado("Prestamos");
	
	public Biblioteca(String nombre) {
		super();
		this.nombre = nombre;
	}
	
	//*****************CREATE*****************
	
	public boolean agregarEjemplar(Escrito escrito, int cantidad) {
		boolean agregado=false;
		if (escrito instanceof Libro) {
			boolean contenido=false;
			for (Libro l:libros) {
				if(l.getISBN().equals(escrito.getISBN())) {
					contenido=true;
				}
			}if (!contenido) {
				libros.add((Libro) escrito);
				agregado=true;
			} 
		}else if (escrito instanceof Articulo) {
			boolean contenido=false;
			for (Articulo a: articulos) {
				if(a.getISBN().equals(escrito.getISBN())) {
					contenido=true;
				}
			}if (!contenido) {
				articulos.add((Articulo) escrito);
				agregado=true;
			}
		}else if (escrito instanceof Revista) {
			boolean contenido=false;
			for (Revista r: revistas) {
				if(r.getISBN().equals(escrito.getISBN())) {
					contenido=true;
				}
			}if (!contenido) {
				Revista rev=(Revista) escrito;
				revistas.add(rev);
				for (EscritoSimple e:rev.getEscritos()) {
					agregarEjemplar(e, 1);
				}
				agregado=true;
			}
		}else if (escrito instanceof Ensayo) {
			boolean contenido=false;
			for (Ensayo e:ensayos) {
				if(e.getISBN().equals(escrito.getISBN())) {
					contenido=true;
				}
			}if (!contenido) {
				ensayos.add((Ensayo) escrito);
				agregado=true;
				
			}
		}
		aumentarEjemplar(escrito, cantidad);
		agregarAutor(escrito);
		agregarCategoria(escrito);
		agregarEditorial(escrito);
		return agregado;
	}
	
	//******************READ******************
	
	public ArrayList<Escrito> materialPrestado(){
		ArrayList<Escrito> prestamos= new ArrayList<>();
		for (Escrito e:this.prestamos.getEscritos()) {
			prestamos.add(e);
		}
		return prestamos;
	}
	
	public ArrayList<String> materialPrestadoTitulos(){
		ArrayList<String> prestamos= new ArrayList<>();
		for (Escrito e:materialPrestado()){
			prestamos.add(e.getTitulo());
		}
		return prestamos;
	}
	
	public ArrayList<String> materialPrestadoISBN(){
		ArrayList<String> prestamos= new ArrayList<>();
		for (Escrito e:materialPrestado()){
			prestamos.add(e.getISBN());
		}
		return prestamos;
	}
	
	public int consultarCantidadPorISBN(String ISBN) {
		int cantidad=-1;
		if(ejemplaresISBN.containsKey(ISBN)) {
			cantidad=ejemplaresISBN.get(ISBN);
		}
		return cantidad;
	}
	
	public int consultarCantidadPorTitulo(String nombre) {
		int cantidad=-1;
		if(ejemplaresTitulo.containsKey(nombre)) {
			cantidad=ejemplaresTitulo.get(nombre);
		}
		return cantidad;
	}
	
	public ArrayList<Escrito> consultarPorISBN(String ISBN) {
		ArrayList<Escrito> escritos = new ArrayList<>();
		for (Libro l:libros) {
			if (l.getISBN().equals(ISBN)) { escritos.add(l);}
		}
		for (Articulo a:articulos) {
			if (a.getISBN().equals(ISBN)) { escritos.add(a);}
		}
		for (Revista r:revistas) {
			if (r.getISBN().equals(ISBN)) { escritos.add(r);}
		}
		for (Ensayo e:ensayos) {
			if (e.getISBN().equals(ISBN)) { escritos.add(e);}
		}
		return escritos;
	}
	
	public ArrayList<Escrito> consultarPorTitulo(String titulo) {
		ArrayList<Escrito> escritos = new ArrayList<>();
		for (Libro l:libros) {
			if (l.getTitulo().equals(titulo)) { escritos.add(l);}
		}
		for (Articulo a:articulos) {
			if (a.getTitulo().equals(titulo)) { escritos.add(a);}
		}
		for (Revista r:revistas) {
			if (r.getTitulo().equals(titulo)) { escritos.add(r);}
		}
		for (Ensayo e:ensayos) {
			if (e.getTitulo().equals(titulo)) { escritos.add(e);}
		}
		return escritos;
	}
	
	public ArrayList<Escrito> consultarPorAutor(String autor){
		ArrayList<Escrito> escritos =new ArrayList<>();
		for (Articulo art:articulos) {
			if (art.getAutor().equals(autor)) {
			escritos.add(art);
			}
		}
		for (Libro l:libros) {
			if (l.getAutor().equals(autor)) {
				escritos.add(l);
				}
		}
		for (Ensayo e:ensayos) {
			if (e.getAutor().equals(autor)) {
				escritos.add(e);
				}
		}
		return escritos;
	}
	
	public ArrayList<Escrito> consultarPorCategoria(String categoria){
		ArrayList<Escrito> escritos =new ArrayList<>();
		
		for (Articulo a:articulos) {
			if (a.getCategoria().equals(categoria)) {
			escritos.add(a);
			}
		}
		for (Libro l:libros) {
			if (l.getCategoria().equals(categoria)) {
				escritos.add(l);
				}
		}
		for (Ensayo e:ensayos) {
			if (e.getCategoria().equals(categoria)) {
				escritos.add(e);
			}
		}
		for (Revista r:revistas) {
			for (String cat : r.getCategorias()) {
				if (cat.equals(categoria)) {
					escritos.add(r);
				}
			}
		}
		return escritos;
	}
	
	public boolean prestarMaterial(Escrito escrito) {
		boolean prestado=false;
		if (ejemplaresISBN.containsKey(escrito.getISBN()) && reducirEjemplar(escrito)) {
			prestamos.agregarMaterial(escrito);
			prestado=true;
		}
		return prestado;
	}
	
	public boolean devolverMateria(Escrito escrito) {
		boolean devuelto=false;
		if (ejemplaresISBN.containsKey(escrito.getISBN()) && this.prestamos.sacarMaterial(escrito)) {
			aumentarEjemplar(escrito,1);
			devuelto=true;
		}
		return devuelto;
	}
	//*****************UPDATE*****************
	
	private boolean agregarAutor(Escrito escrito) {
		boolean agregado=false;
		if (!(escrito instanceof Revista)) {
			EscritoSimple esc=(EscritoSimple) escrito;
			if (!autores.contains(esc.getAutor())){
				autores.add(esc.getAutor());
				agregado=true;
			}
		}
		return agregado;
	}
	
	private boolean agregarCategoria(Escrito escrito) {
		boolean agregado=false;
		if (!(escrito instanceof Revista)) {
			EscritoSimple esc=(EscritoSimple) escrito;
			if (!categorias.contains(esc.getCategoria())){
				categorias.add(esc.getCategoria());
				agregado=true;
			}
		}else if (escrito instanceof Revista) {
			Revista rev=(Revista) escrito;
			for (String c: rev.getCategorias()) {
				if (!categorias.contains(c)){
					categorias.add(c);
				}
			}
			agregado=true;
		}
		return agregado;
	}
	
	private boolean agregarEditorial(Escrito escrito) {
		boolean agregado=false;
		if (escrito instanceof Revista) {
			Revista revista=(Revista) escrito;
			if (!editoriales.contains(revista.getEditorial())){
				editoriales.add(revista.getEditorial());
				agregado=true;
			}
		}else if (escrito instanceof Libro) {
			Libro libro=(Libro) escrito;
			if (!editoriales.contains(libro.getEditorial())){
				editoriales.add(libro.getEditorial());
				agregado=true;
			}
		}
		return agregado;
	}
	
	private boolean reducirEjemplar(Escrito escrito) {
		boolean reducido=false;
		if (ejemplaresISBN.containsKey(escrito.getISBN())) {
			if (escrito instanceof Articulo) {
				if (ejemplaresISBN.get(escrito.getISBN())>1) {
					ejemplaresISBN.put(escrito.getISBN(), ejemplaresISBN.get(escrito.getISBN())-1);
					ejemplaresTitulo.put(escrito.getTitulo(), ejemplaresTitulo.get(escrito.getTitulo())-1);
					reducido = true;
				}
			}else {
				ejemplaresISBN.put(escrito.getISBN(), ejemplaresISBN.get(escrito.getISBN())-1);
				ejemplaresTitulo.put(escrito.getTitulo(), ejemplaresTitulo.get(escrito.getTitulo())-1);
				reducido = true;
			}
		}
		return reducido;
	}
	
	private boolean aumentarEjemplar(Escrito escrito, int cantidad) {
		boolean aumentado=false;
		if (ejemplaresISBN.containsKey(escrito.getISBN())) {
			ejemplaresISBN.put(escrito.getISBN(), ejemplaresISBN.get(escrito.getISBN())+cantidad);
			ejemplaresTitulo.put(escrito.getTitulo(), ejemplaresTitulo.get(escrito.getTitulo())+cantidad);
			aumentado=true;
		}else {
			ejemplaresISBN.put(escrito.getISBN(), cantidad);
			ejemplaresTitulo.put(escrito.getTitulo(), cantidad);
			aumentado=true;
		}
		return aumentado;
	}
	
	//*****************DELETE*****************
	
	public boolean eliminarMaterial(Escrito escrito) {
		boolean eliminado=false;
		if (ejemplaresISBN.containsKey(escrito.getISBN())) {
			if (escrito instanceof Libro) {
				libros.remove((Libro) escrito);
				eliminado=true;
			}
			else if(escrito instanceof Revista) {
				revistas.remove((Revista) escrito);
				eliminado=true;
			}
			else if(escrito instanceof Articulo) {
				articulos.remove((Articulo) escrito);
				eliminado=true;
			}
			else if(escrito instanceof Ensayo) { 
				ensayos.remove((Ensayo) escrito);
				eliminado=true;
			}
		} 
		return eliminado;
	}
	
	
	//**********Getters and Setters**********
	
	public String getNombre() {
		return nombre;
	}
	public ArrayList<String> getAutores() {
		return autores;
	}
	public ArrayList<Libro> getLibros() {
		return libros;
	}
	public ArrayList<Articulo> getArticulos() {
		return articulos;
	}
	public ArrayList<Ensayo> getEnsayos() {
		return ensayos;
	}
	public ArrayList<Revista> getRevistas() {
		return revistas;
	}
	public ArrayList<String> getEditoriales() {
		return editoriales;
	}
	public ArrayList<String> getCategorias() {
		return categorias;
	}
	public HashMap<String, Integer> getEjemplaresISBN() {
		return ejemplaresISBN;
	}
	public HashMap<String, Integer> getEjemplaresTitulo() {
		return ejemplaresTitulo;
	}
	public Prestado getPrestamos() {
		return this.prestamos;
	}
}
