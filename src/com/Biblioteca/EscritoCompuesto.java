package com.Biblioteca;

public class EscritoCompuesto extends Escrito{
	private String editorial; 
	private String numero;
	/**
	 * El número corresponde al valor de la sucesión de las revistas; por
	 * ejemplo "2016_02" o "Febrero-2015"; 
	 */
	
	public EscritoCompuesto(String titulo, String iSBN, String editorial, String numero) {
		super(titulo, iSBN);
		this.editorial = editorial;
		this.numero = numero;
	}
	public String getEditorial() {
		return editorial;
	}
	public String getNumero() {
		return numero;
	}
}
