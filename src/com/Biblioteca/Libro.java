package com.Biblioteca;

public class Libro extends EscritoSimple {
	private String editorial;

	public Libro(String titulo, String iSBN, String autor, String categoria, String editorial) {
		super(titulo, iSBN, autor, categoria);
		this.editorial = editorial;
	}

	public String getEditorial() {
		return editorial;
	}	
	
}
