package com.Biblioteca;

import java.util.ArrayList;

public class Revista extends EscritoCompuesto{	
	
	private ArrayList<String> categorias = new ArrayList<>();
	private ArrayList<String> autores = new ArrayList<>();
	private ArrayList<EscritoSimple> escritos = new ArrayList<>();
	

	public Revista(String titulo, String iSBN, String editorial, String numero) {
		super(titulo, iSBN, editorial, numero);
	} 

	public boolean agregarEscrito(EscritoSimple escrito) {
		boolean agregado = false;
		if(!comprobarExistenciaEscrito(escrito) && (escrito instanceof Articulo || escrito instanceof Ensayo)) {
			escritos.add(escrito);
			agregarCategoria(escrito.getCategoria());
			agregarAutor(escrito.getAutor());
			agregado =true;
		}
		return agregado;
	}
	
	private void agregarCategoria(String categoria) { 
		if (!categorias.contains(categoria)) {
			categorias.add(categoria);
		}
	}
	
	private void agregarAutor(String autor) {
		if (!autores.contains(autor)) {
			autores.add(autor);
		}
	}
	
	private boolean comprobarExistenciaEscrito(EscritoSimple escrito) {
		boolean existe=false;
		for(EscritoSimple e: escritos) {
			if(e.getISBN().equals(escrito.getISBN())) {
				existe=true;
				break;
			}
		}
		return existe;
	}
	

	public ArrayList<String> getCategorias() {
		return categorias;
	}

	public ArrayList<String> getAutores() {
		return autores;
	}

	public ArrayList<EscritoSimple> getEscritos() {
		return escritos;
	}
	
}