package com.Principal;

import com.Biblioteca.Articulo;
import com.Biblioteca.Biblioteca;
import com.Biblioteca.Ensayo;
import com.Biblioteca.Escrito;
import com.Biblioteca.Libro;
import com.Biblioteca.Revista;
import com.Ventanas.Mostrar;

public class Principal {

	public static void main(String[] args) {
		Biblioteca biblioteca = new Biblioteca("Biblioteca Gabo");
		biblioteca=inicializarBiblioteca(biblioteca);
		//Llamando la ventana principal
		boolean continuar=true;
		while(continuar) {
			switch (Mostrar.ventanaPrincipal(biblioteca)) {
			case 0:
				//Agregar Escrito
				switch (Mostrar.pedirTipoEscrito()) {
				case "Libro":
					biblioteca=Mostrar.pedirDatosLibro(biblioteca);
					break;
				case "Revista":
					break;
				case "Articulo":
					break;
				case "Ensayo":
					break;
				default:
					break;
				}
				break;
			case (int) 1:
				//Consultar Escrito
				break;
			case 2:
				//Salir
				if (Mostrar.showConfirmDialog(null, "¿Estás seguro de salir?", "Salir", 0, 3, Mostrar.gaboIcon())==Mostrar.OK_OPTION) {
					continuar=false;
				}
				break;
			case -1:
				//-1 : cerrar ventana
				continuar=false;
				break;
		default:
			break;
		}
		}
		
	}
	
	/*********************OTROS MÉTODOS DE INICIALIZACÓN*********************/
	
	public static Biblioteca probarBiblioteca(Biblioteca biblioteca) {

		for (String a:biblioteca.getAutores()) {
			System.out.println("Autor --> "+a);
		}
		for (String c: biblioteca.getCategorias()) {
			System.out.println("Categoria --> "+c);
		}
		for (String e : biblioteca.getEditoriales()) {
			System.out.println("Editorial --> "+e);
		}
		for (Libro l : biblioteca.getLibros()) {
			System.out.println("Libro --> "+l.getTitulo());
		}
		
		System.out.println("Cantidad iliada --> "+biblioteca.consultarCantidadPorTitulo("La Iliada"));
		
		Escrito prestamo = biblioteca.consultarPorTitulo("La Iliada").get(0);
		System.out.println("Prestando --> "+ biblioteca.prestarMaterial(prestamo));	
		
		
		System.out.println("Consultando material prestado ...");
		for (Escrito e:biblioteca.materialPrestado()) {
			System.out.println("Prestado "+e.getTitulo());
		}
		
		System.out.println("Consultando cantidades prestadas");
		
		for (Escrito e:biblioteca.getPrestamos().getEscritos()) {
			System.out.println(e.getTitulo()+" : "+biblioteca.getPrestamos().consultarCantidadPrestada(e)+" unidades prestadas");
		}
		
		System.out.println("Cantidad iliada --> "+biblioteca.consultarCantidadPorTitulo("La Iliada"));
		
		System.out.println("Contidad el Quijote --> " + biblioteca.consultarCantidadPorISBN("8441405288"));
		
		
		System.out.println("Consultando por Autores ...");
		System.out.println("Consultando a Gabriel García Márquez");
		for (Escrito e:biblioteca.consultarPorAutor("Gabriel García Márquez")) {
			System.out.println("--> "+e.getTitulo());
		}
		
		System.out.println("Consultando a Julio Cortázar");
		for (Escrito e:biblioteca.consultarPorAutor("Julio Cortázar")) {
			System.out.println("--> "+e.getTitulo());
		}
		
		
		System.out.println("Consultando por categoría...");
		System.out.println("Categoría Crítica...");
		for (Escrito e:biblioteca.consultarPorCategoria("Crítica")) {
			System.out.println("--> "+e.getTitulo());
		}
		
		System.out.println("Categoría Realismo Mágico...");
		for (Escrito e:biblioteca.consultarPorCategoria("Realismo Mágico")) {
			System.out.println("--> "+e.getTitulo());
		}
		
		
		System.out.println("Fin de las pruebas");
		return biblioteca;
	}
	
	public static Biblioteca inicializarBiblioteca(Biblioteca biblioteca) {
		
		/******************************LIBROS******************************/
		
		Libro cienAñosDeSoledad = new Libro("Cien años de soledad", "9968411302", "Gabriel García Márquez", "Realismo Mágico", "Sudamericana");
		Libro elQuijote = new Libro("El Quijote de la Mancha", "8441405288", "Miguel de Cervantes", "Aventura Épica", "Francísco de Robles");
		Libro iliada = new Libro("La Iliada", "93487523234", "Homero", "Aventura Épica",  "Fondo de Cultura Económica");
		
		
		/******************************ARTICULOS******************************/
		
		Articulo ciaoVerona = new Articulo("Ciao, Verona", "873645726", "Julio Cortázar", "Crítica");
		Articulo conductaEnLosVelorios = new Articulo("Conducta en los velorios", "984753234", "Julio Cortázar", "Crítica");
		Articulo advertenciaDeUnEscritor = new Articulo("Advertencia de un Escritor", "01240854", "Gabriel García Márquez", "Cultura");
		
		/******************************ENSAYOS******************************/
		
		Ensayo ensayoSobreLaCegera = new Ensayo("Ensayo sobre la ceguera", "8420428655", "José Saramago", "Crítica");
		Ensayo hayQueSerRealmenteIdiotaPara = new Ensayo("Hay que ser realmente idiota para...", "82736423", "Julio Cortázar", "Crítica");
		
		/******************************REVISTAS******************************/
		
		Revista babelia = new Revista("Babelia", "098054632", "Sudamericana", "432");
		Revista natGeo = new Revista("National Geographic", "98273598273", "Televisa", "2017-01");
		
			/***********AGREGANDO CONTENIDO A LAS REVISTAS************/
		
		babelia.agregarEscrito(ciaoVerona);
		babelia.agregarEscrito(conductaEnLosVelorios);
		babelia.agregarEscrito(ensayoSobreLaCegera);
		babelia.agregarEscrito(advertenciaDeUnEscritor);
		
				
		/******************************AGREGANDO CONTENIDO******************************/
		
		biblioteca.agregarEjemplar(elQuijote, 5);
		biblioteca.agregarEjemplar(cienAñosDeSoledad, 3);
		biblioteca.agregarEjemplar(babelia, 3); 
		biblioteca.agregarEjemplar(natGeo, 3);
		biblioteca.agregarEjemplar(iliada, 6);
		biblioteca.agregarEjemplar(hayQueSerRealmenteIdiotaPara, 6);
		
		/******************************CONSULTANDO ESCRITOS DE UN AUTOR******************************/
		
		
		biblioteca=probarBiblioteca(biblioteca);
		return biblioteca;
		
	}
	
}
