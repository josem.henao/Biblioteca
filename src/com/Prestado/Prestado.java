package com.Prestado;

import java.util.ArrayList;
import java.util.HashMap;

import com.Biblioteca.Escrito;

public class Prestado{
	String nombre;
	ArrayList<Escrito> escritos = new ArrayList<>();
	HashMap<String, Integer> cantidadEscritos = new HashMap<>();

	public Prestado(String nombre) { 
		this.nombre=nombre;
	}

	public boolean agregarMaterial(Escrito escrito) {
		boolean agregado=false;
		if (escritos.add(escrito)) {
			agregado = true;
			aumentarCantidad(escrito);
		}
		return agregado;
		
	}
	
	public boolean sacarMaterial(Escrito escrito) {
		boolean devuelto=false;
		if (escritos.remove(escrito) && disminuirCantidad(escrito)) {
			devuelto=true;
		}
		return devuelto;
	}
	
	private void aumentarCantidad(Escrito escrito) {
		if (cantidadEscritos.containsKey(escrito.getISBN())) {
			cantidadEscritos.put(escrito.getISBN(), cantidadEscritos.get(escrito.getISBN())+1);
		}else {
			cantidadEscritos.put(escrito.getISBN(), 1);
		}
		
	}
	
	private boolean disminuirCantidad(Escrito escrito) {
		boolean disminuido = false;
		if (cantidadEscritos.containsKey(escrito.getISBN()) && cantidadEscritos.get(escrito.getISBN())>0) {
			cantidadEscritos.put(escrito.getISBN(), cantidadEscritos.get(escrito.getISBN())-1);
			disminuido=true;
		}
		return disminuido;
	}
	
	public int consultarCantidadPrestada(Escrito escrito) {
		int cantidad = -1;
		if (cantidadEscritos.containsKey(escrito.getISBN())) {
			cantidad = cantidadEscritos.get(escrito.getISBN());
		}
		return cantidad;
	}
	
	public ArrayList<Escrito> getEscritos(){
		return escritos;
	}
}
