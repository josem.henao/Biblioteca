package com.Test;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

import com.Biblioteca.Articulo;
import com.Biblioteca.Biblioteca;
import com.Biblioteca.Ensayo;
import com.Biblioteca.Escrito;
import com.Biblioteca.EscritoSimple;
import com.Biblioteca.Libro;
import com.Biblioteca.Revista;

public class BibliotecaTest {
	
	
	/***************************LibroTest()**************************/
	
	@Test
	public void ingresarLibroSinTenerLibrosIngresadosTest() {
		//Arrange
		Biblioteca bibliotecaGabo = new Biblioteca("BibliotecaTest");
		Escrito iliada= new Libro("La Iliada", "2934682364", "Homero", "Aventura Épica", "Oveja Negra");
		//Act
		boolean bool= bibliotecaGabo.agregarEjemplar(iliada, 5);
		//Assert
		Assert.assertTrue(bool); 
	}
	
	@Test
	public void ingresarLibroTeniendoLibrosIngresadosTest() {
		//Arrange		
		Biblioteca bibliotecaGabo = new Biblioteca("BibliotecaTest");
		
		Escrito iliada = new Libro("La Iliada", "2934682364", "Homero", "Aventura Épica", "Oveja Negra");
		Escrito cienAgnos = new Libro("Cien Años de soledad", "273547623", "Gabriel García Márquez", "Realismo Mágico", "Sudamericana");
		//Act
		bibliotecaGabo.agregarEjemplar(iliada, 5);
		boolean bool= bibliotecaGabo.agregarEjemplar(cienAgnos, 4);
		//Assert
		Assert.assertTrue(bool);
	}
	
	@Test
	public void ingresarLibroQueYaSeHabiaIngresadoTest() {
		//Arrange		
		Biblioteca bibliotecaGabo = new Biblioteca("BibliotecaTest");
		
		Escrito cienAgnos = new Libro("Cien Años de soledad", "273547623", "Gabriel García Márquez", "Realismo Mágico", "Sudamericana");
		//Act
		bibliotecaGabo.agregarEjemplar(cienAgnos, 5);
		boolean bool= bibliotecaGabo.agregarEjemplar(cienAgnos, 4); 
		//Assert
	
		Assert.assertFalse(bool);

	}
	
	@Test
	public void obtenerNumeroDeLibrosPorTituloSinHaberlosIngresadoTest() {
		//Arrange
		Biblioteca bibliotecaGabo = new Biblioteca("BibliotecaTest");
		Escrito cienAgnos = new Libro("Cien Años de soledad", "273547623", "Gabriel García Márquez", "Realismo Mágico", "Sudamericana");
		//Act
		bibliotecaGabo.agregarEjemplar(cienAgnos, 5);
		int cantidad = bibliotecaGabo.consultarCantidadPorTitulo("La iliada");
		//Assert
	
		Assert.assertEquals(-1, cantidad);
	}
	
	@Test
	public void obtenerNumeroDeLibrosPorNombreTest() {
		//Arrange
		Biblioteca bibliotecaGabo = new Biblioteca("BibliotecaTest");
		Escrito cienAgnos = new Libro("Cien años de soledad", "273547623", "Gabriel García Márquez", "Realismo Mágico", "Sudamericana");
		//Act
		bibliotecaGabo.agregarEjemplar(cienAgnos, 5);
		int cantidad = bibliotecaGabo.consultarCantidadPorTitulo("Cien años de soledad");
		//Assert
	
		Assert.assertEquals(5, cantidad);
	}
	
	@Test
	public void obtenerNumeroDeLibrosPorISBNSinHaberlosIngresadoTest() {
		//Arrange
		Biblioteca bibliotecaGabo = new Biblioteca("BibliotecaTest");
		Escrito cienAgnos = new Libro("Cien Años de soledad", "273547623", "Gabriel García Márquez", "Realismo Mágico", "Sudamericana");
		//Act
		bibliotecaGabo.agregarEjemplar(cienAgnos, 5);
		int cantidad = bibliotecaGabo.consultarCantidadPorISBN("858748569");
		//Assert
	
		Assert.assertEquals(-1, cantidad);
	}
	
	@Test
	public void obtenerNumeroDeLibrosPorISBNTest() {
		//Arrange
		Biblioteca bibliotecaGabo = new Biblioteca("BibliotecaTest");
		Escrito cienAgnos = new Libro("Cien Años de soledad", "273547623", "Gabriel García Márquez", "Realismo Mágico", "Sudamericana");
		//Act
		bibliotecaGabo.agregarEjemplar(cienAgnos, 5);
		int cantidad = bibliotecaGabo.consultarCantidadPorISBN("273547623");
		//Assert
	
		Assert.assertEquals(5, cantidad);
	}
	
	
	/********************************RevistaTest()********************************/
	
	
	@Test
	public void ingresarRevistaSinTenerRevistasIngresadasTest() {
		
		//Arrange
		Biblioteca bibliotecaGabo = new Biblioteca("BibliotecaTest");
		
		Escrito babelia = new Revista("Babelia", "098054632", "Sudamericana", "432");
		
		
		//Act
		boolean bool= bibliotecaGabo.agregarEjemplar(babelia, 2);
		
		//Assert
		Assert.assertTrue(bool); 
	}
	
	@Test
	public void ingresarRevistaTeniendoRevistasIngresadasTest() {
		
		//Arrange		
		Biblioteca bibliotecaGabo = new Biblioteca("BibliotecaTest");
		
		Escrito ciaoVerona = new Articulo("Ciao, Verona", "873645726", "Julio Cortázar", "Crítica");
		Escrito conductaEnLosVelorios = new Articulo("Conducta en los velorios", "984753234", "Julio Cortázar", "Crítica");
		Revista babelia = new Revista("Babelia", "098054632", "Sudamericana", "432");
		
		babelia.agregarEscrito((EscritoSimple) conductaEnLosVelorios);
		Revista natGeo = new Revista("National Geographic", "98273598273", "Televisa", "2017-01");
		natGeo.agregarEscrito((EscritoSimple) ciaoVerona);
		
		bibliotecaGabo.agregarEjemplar(babelia, 3);
		//Act
		boolean bool= bibliotecaGabo.agregarEjemplar(natGeo, 2);
		
		//Assert
		Assert.assertTrue(bool); 
	}
	
	@Test
	public void ingresarRevistaQueYaSeHabiaIngresadoTest() {
		//Arrange
		Biblioteca bibliotecaGabo = new Biblioteca("BibliotecaTest");
		
		Articulo advertenciaDeUnEscritor = new Articulo("Advertencia de un Escritor", "01240854", "Gabriel García Márquez", "Cultura");
		Revista babelia = new Revista("Babelia", "098054632", "Sudamericana", "432");
		babelia.agregarEscrito(advertenciaDeUnEscritor);
		bibliotecaGabo.agregarEjemplar(babelia, 3);
		
		//Act
		boolean bool= bibliotecaGabo.agregarEjemplar(babelia, 4); 
		
		//Assert
		Assert.assertFalse(bool);

	}
	
	/********************************EnsayoTest()********************************/
	
	
	@Test
	public void ingresarEnsayoSinTenerEnsayosIngresadosTest() {
		
		//Arrange
		Biblioteca bibliotecaGabo = new Biblioteca("BibliotecaTest");
		Ensayo ensayoSobreLaCegera = new Ensayo("Ensayo sobre la ceguera", "8420428655", "José Saramago", "Crítica");
		
		//Act
		boolean bool=bibliotecaGabo.agregarEjemplar(ensayoSobreLaCegera, 3);
		
		//Assert
		Assert.assertTrue(bool); 
	}
	
	@Test
	public void ingresarEnsayoTeniendoEnsayosIngresadosTest() {
		
		//Arrange
		Biblioteca bibliotecaGabo = new Biblioteca("BibliotecaTest");
		Ensayo ensayoSobreLaCegera = new Ensayo("Ensayo sobre la ceguera", "8420428655", "José Saramago", "Crítica");
		Ensayo hayQueSerRealmenteIdiotaPara = new Ensayo("Hay que ser realmente idiota para...", "82736423", "Julio Cotázar", "Crítica");
		bibliotecaGabo.agregarEjemplar(ensayoSobreLaCegera, 3);
		//Act
		boolean bool=bibliotecaGabo.agregarEjemplar(hayQueSerRealmenteIdiotaPara, 6);
		
		//Assert
		Assert.assertTrue(bool); 
	}
	
	@Test
	public void ingresarEnsayoQueYaSeHabiaIngresadoTest() {
		//Arrange
		Biblioteca bibliotecaGabo = new Biblioteca("BibliotecaTest");
		Ensayo ensayoSobreLaCegera = new Ensayo("Ensayo sobre la ceguera", "8420428655", "José Saramago", "Crítica");
		bibliotecaGabo.agregarEjemplar(ensayoSobreLaCegera, 3);
		//Act
		boolean bool=bibliotecaGabo.agregarEjemplar(ensayoSobreLaCegera, 6);
		
		//Assert
		Assert.assertFalse(bool); 

	}
	
	
/********************************ArticuloTest()********************************/
	
	
	@Test
	public void ingresarArticuloSinTenerArtculosIngresadosTest() {
		
		//Arrange
		Biblioteca bibliotecaGabo = new Biblioteca("BibliotecaTest");
		Articulo conductaEnLosVelorios = new Articulo("Conducta en los velorios", "984753234", "Julio Cortázar", "Crítica");
		
		//Act
		boolean bool=bibliotecaGabo.agregarEjemplar(conductaEnLosVelorios, 3);
		
		//Assert
		Assert.assertTrue(bool); 
	}
	
	
	@Test
	public void ingresarArticuloTeniendoArticulosIngresadosTest() {
		
		//Arrange
		Biblioteca bibliotecaGabo = new Biblioteca("BibliotecaTest");
		Articulo conductaEnLosVelorios = new Articulo("Conducta en los velorios", "984753234", "Julio Cortázar", "Crítica");
		Articulo advertenciaDeUnEscritor = new Articulo("Advertencia de un Escritor", "01240854", "Gabriel García Márquez", "Cultura");
		bibliotecaGabo.agregarEjemplar(advertenciaDeUnEscritor, 3);
		//Act
		boolean bool=bibliotecaGabo.agregarEjemplar(conductaEnLosVelorios, 3);
				
		//Assert
		Assert.assertTrue(bool); 
	}
	
	@Test
	public void ingresarArticuloQueYaSeHabiaIngresadoTest() {
		
		//Arrange
		Biblioteca bibliotecaGabo = new Biblioteca("BibliotecaTest");
		Articulo conductaEnLosVelorios = new Articulo("Conducta en los velorios", "984753234", "Julio Cortázar", "Crítica");
		bibliotecaGabo.agregarEjemplar(conductaEnLosVelorios, 3);

		//Act
		boolean bool=bibliotecaGabo.agregarEjemplar(conductaEnLosVelorios, 3);
						
		//Assert
		Assert.assertFalse(bool); 

	}
	
	/****************************PRESTAMOS TEST****************************/
	
	@Test
	public void pedirEscritoPrestadoTest() {
		//Arrange
		Biblioteca bibliotecaGabo = new Biblioteca("BibliotecaTest");
		
		Articulo advertenciaDeUnEscritor = new Articulo("Advertencia de un Escritor", "01240854", "Gabriel García Márquez", "Cultura");
		Revista babelia = new Revista("Babelia", "098054632", "Sudamericana", "432");
		babelia.agregarEscrito(advertenciaDeUnEscritor);
		bibliotecaGabo.agregarEjemplar(babelia, 3);
		
		//Act
		boolean bool = bibliotecaGabo.prestarMaterial(babelia);
		
		//Assert
		Assert.assertTrue(bool);

	}
	@Test
	public void pedirEscritoPrestadoConEjemplaresPrestadosTest() {
		//Arrange
		Biblioteca bibliotecaGabo = new Biblioteca("BibliotecaTest");
		
		Articulo advertenciaDeUnEscritor = new Articulo("Advertencia de un Escritor", "01240854", "Gabriel García Márquez", "Cultura");
		Revista babelia = new Revista("Babelia", "098054632", "Sudamericana", "432");
		babelia.agregarEscrito(advertenciaDeUnEscritor);
		bibliotecaGabo.agregarEjemplar(babelia, 3);
		bibliotecaGabo.prestarMaterial(babelia);
		
		//Act
		boolean bool = bibliotecaGabo.prestarMaterial(babelia);
		
		//Assert
		Assert.assertTrue(bool);

	}
	@Test
	public void devolverMaterialPrestadoTest() {
		//Arrange
		Biblioteca bibliotecaGabo = new Biblioteca("BibliotecaTest");
		Articulo advertenciaDeUnEscritor = new Articulo("Advertencia de un Escritor", "01240854", "Gabriel García Márquez", "Cultura");
		Revista babelia = new Revista("Babelia", "098054632", "Sudamericana", "432");
		babelia.agregarEscrito(advertenciaDeUnEscritor);
		bibliotecaGabo.agregarEjemplar(babelia, 3);
		bibliotecaGabo.prestarMaterial(babelia);
		
		//Act
		boolean bool = bibliotecaGabo.devolverMateria(babelia);
		
		//Assert
		Assert.assertTrue(bool);

	}
	@Test
	public void consultarLaCantidadPrestadaTest() {
		//Arrange
		Biblioteca bibliotecaGabo = new Biblioteca("BibliotecaTest");
		Articulo advertenciaDeUnEscritor = new Articulo("Advertencia de un Escritor", "01240854", "Gabriel García Márquez", "Cultura");
		Revista babelia = new Revista("Babelia", "098054632", "Sudamericana", "432");
		babelia.agregarEscrito(advertenciaDeUnEscritor);
		bibliotecaGabo.agregarEjemplar(babelia, 3);
		bibliotecaGabo.prestarMaterial(babelia);
		
		//Act
		int cantidad=bibliotecaGabo.getPrestamos().consultarCantidadPrestada(babelia);
		
		//Assert
		Assert.assertEquals(1, cantidad);

	}
	@Test
	public void consultarLosEscritosPrestados() {
		//Arrange
		Biblioteca bibliotecaGabo = new Biblioteca("BibliotecaTest");
		
		Articulo advertenciaDeUnEscritor = new Articulo("Advertencia de un Escritor", "01240854", "Gabriel García Márquez", "Cultura");
		Revista babelia = new Revista("Babelia", "098054632", "Sudamericana", "432");
		babelia.agregarEscrito(advertenciaDeUnEscritor);
		
		bibliotecaGabo.agregarEjemplar(babelia, 3);
		bibliotecaGabo.agregarEjemplar(advertenciaDeUnEscritor, 5);
		bibliotecaGabo.prestarMaterial(babelia);
		bibliotecaGabo.prestarMaterial(advertenciaDeUnEscritor);
		
		ArrayList<Escrito> escritosPrestados = new ArrayList<>();
		escritosPrestados.add(babelia);
		escritosPrestados.add(advertenciaDeUnEscritor);
		//Act
		ArrayList<Escrito> prestamos=new ArrayList<>();
		prestamos=bibliotecaGabo.materialPrestado();
		
		//Assert
		Assert.assertEquals(escritosPrestados, prestamos);

	}
	
	
}
